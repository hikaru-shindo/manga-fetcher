<?php
namespace MangaFetcher\Provider;

use Goutte\Client;

abstract class AbstractUrlProvider implements UrlProviderInterface
{
    /**
     * @var \Goutte\Client
     */
    private $client;

    private $url;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param Client $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }
}
