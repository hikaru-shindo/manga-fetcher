<?php
namespace MangaFetcher\Provider;

use Symfony\Component\DomCrawler\Crawler;

class MangareaderUrlProvider extends AbstractUrlProvider
{
    /**
     * @var array
     */
    private $chapters = null;

    /**
     * @var array
     */
    private $urls = array();

    /**
     * @var array
     */
    private $names = array();

    /**
     * {@inheritdoc}
     */
    public function supports($url)
    {
        return false !== strpos($url, 'mangareader.net');
    }

    /**
     * @param string $url
     * @return $this
     */
    public function setUrl($url)
    {
        parent::setUrl($url);
        $this->chapters = null;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getChapters()
    {
        $this->initChapterValues();

        return $this->chapters;
    }

    /**
     * {@inheritdoc}
     */
    public function getChapter($num)
    {
        $num = (int) $num;

        if (in_array($num, $this->getChapters())) {
            $chapterUrl = $this->urls[$num];
            $images = array();

            return $this->generateImageArrayRecursive($chapterUrl, $num, $images);
        }

        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function getChapterName($num)
    {
        $num = (int) $num;

        $this->initChapterValues();
        return empty($this->names[$num]) ? sprintf('%04d', $num) : $this->names[$num];
    }

    /**
     * @param $url
     * @param $num
     * @param array $images
     */
    protected function generateImageArrayRecursive($url, $num, array &$images)
    {
        $crawler = $this->getClient()->request('get', $url);
        $imageHolder = $crawler->filter('#imgholder a');

        $href = $imageHolder->attr('href');

        if (empty($href)) {
            // Bail out! Error chapter!
            return array();
        }

        if ($this->getChapterNumber($href) !== $num) {
            return $images;
        }

        $image = $this->getImageUrl($imageHolder);
        if (false !== $image) {
            $images[] = $image;
        }

        return $this->generateImageArrayRecursive($imageHolder->attr('href'), $num, $images);
    }

    /**
     * @param Crawler $crawler
     * @return string|false
     */
    protected function getImageUrl(Crawler $crawler)
    {
        $image = $crawler->filter('img');

        if ($image->count() > 0) {
            return $image->attr('src');
        }

        return false;
    }

    protected function initChapterValues()
    {
        if (null === $this->chapters) {
            $chapters = array();
            $urls = array();
            $names = array();

            $crawler = $this->getClient()->request('get', $this->getUrl());

            $crawler->filter('#chapterlist #listing td a')
                ->each(
                    function (\Symfony\Component\DomCrawler\Crawler $node) use (&$names, &$chapters, &$urls) {
                        $url = $node->attr('href');
                        $chapter = $this->getChapterNumber($url);

                        $chapters[$chapter] = $chapter;
                        $urls[$chapter] = $url;
                        $names[$chapter] = $node->text();
                    }
                )
            ;

            $this->urls = $urls;
            $this->chapters = $chapters;
        }
    }

    /**
     * Get chapter number from href
     *
     * @param string $href
     * @return int
     */
    protected function getChapterNumber($href)
    {
        $split = explode('/',$href);
        if (false !== strpos($href, '.html')) {
            $split = explode('-', end($split));
            $chapNum = end($split);
            $chapNum = (int) str_replace('.html','',$chapNum);
        } else {
            $chapNum = $split[count($split) - 2];
            if (!is_numeric($chapNum)) {
                $chapNum = end($split);
            }
            $chapNum = (int) $chapNum;
        }

        return $chapNum;
    }
}
