<?php
namespace MangaFetcher\Provider;

interface UrlProviderInterface
{
    /**
     * Set base url
     *
     * @param $url
     * @return $this
     */
    public function setUrl($url);

    /**
     * Check if plugin supports this url
     *
     * @param string $url
     * @return boolean
     */
    public function supports($url);

    /**
     * List of available chapter numbers.
     *
     * @return array
     */
    public function getChapters();

    /**
     * Get list of image urls for chapter $num.
     *
     * @param $num
     * @return array
     */
    public function getChapter($num);

    /**
     * Get text string as name for chapter $num.
     *
     * @param $num
     * @return string
     */
    public function getChapterName($num);
}
