<?php
namespace MangaFetcher\Command;

use MangaFetcher\Exception\NoProviderFoundException;
use MangaFetcher\Service\FetcherService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class FetchCommand extends Command
{
    /**
     * @var \MangaFetcher\Service\FetcherService
     */
    private $service;

    public function __construct(FetcherService $fetcherService)
    {
        parent::__construct();
        $this->service = $fetcherService;
    }

    protected function configure()
    {
        $this
            ->setName('manga:fetch')
            ->addArgument(
                'config',
                InputArgument::REQUIRED,
                'Which configuration yml to use?'
            )
            ->addOption(
                'path',
                'p',
                InputOption::VALUE_REQUIRED,
                'Save path for download',
                getcwd() . '/files')
            ->addOption(
                'zip',
                'z',
                InputOption::VALUE_NONE,
                'Archive downloaded chapters'
            )
            ->setDescription('Fetch configured manga')
        ;
    }

    protected function getConfig($configFile)
    {
        if (!is_file($configFile) || !is_readable($configFile)) {
            return false;
        }

        return Yaml::parse(file_get_contents($configFile), true);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $savePath   = $input->getOption('path');
        $configFile = $input->getArgument('config');
        $zip        = $input->getOption('zip');
        try {
            $config = $this->getConfig($configFile);
        } catch (ParseException $e) {
            $output->writeln('<error>Could not parse configuration file.</error>');
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
            return;
        }

        if ($config === false) {
            $output->writeln('<error>No such configuration file or not readable</error>');
            return;
        }

        $service = $this->service;

        foreach ($config as $title => $manga) {
            $service->setSavePath($savePath);
            if (empty($manga['url'])) {
                $output->writeln(sprintf('<error>Could not load manga %s. No url given</error>', $title));
                break;
            }

            $start = empty($manga['start']) ? null : (int) $manga['start'];
            $end   = empty($manga['end']) ? null : (int) $manga['end'];

            $output->writeln(sprintf('Fetching %s', $title));

            $service->setSavePath($service->getSavePath() . '/' . $title);
            try {
                $service->fetch($manga['url'], $start, $end, $zip);
            } catch (NoProviderFoundException $e) {
                $output->writeln(sprintf('<error>Cannot download %s, no provider found.</error>', $title));
            }
        }
    }
}
