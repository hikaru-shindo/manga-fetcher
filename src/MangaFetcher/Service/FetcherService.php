<?php
namespace MangaFetcher\Service;

use Goutte\Client;
use MangaFetcher\Exception\NoProviderFoundException;
use MangaFetcher\Provider\UrlProviderInterface;
use Symfony\Component\Filesystem\Filesystem;
use GuzzleHttp\Exception\ClientException as HttpClientException;

class FetcherService
{
    /**
     * Collection of all registered providers
     *
     * @var array
     */
    protected $providers = array();

    /**
     * @var string
     */
    protected $savePath = 'files';

    /**
     * @var \Goutte\Client
     */
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Add a provider to the service
     *
     * @param UrlProviderInterface $provider
     * @return $this
     */
    public function addProvider(UrlProviderInterface $provider)
    {
        if (!in_array($provider, $this->providers, true)) {
            $this->providers[] = $provider;
        }

        return $this;
    }

    /**
     * Remove a provider from the service
     *
     * @param UrlProviderInterface $provider
     * @return $this
     */
    public function removeProvider(UrlProviderInterface $provider)#
    {
        $provider = array_search($provider, $this->providers, true);
        if ($provider) {
            unset($this->providers[$provider]);
        }

        return $this;
    }

    /**
     * Get all registered providers on the service
     *
     * @return array
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * Fetch given manga using providers
     *
     * @param string $url
     * @param int $startChapter
     * @param int $endChapter
     * @param bool $zip
     * @throws \MangaFetcher\Exception\NotImplementedException
     */
    public function fetch($url, $startChapter = null, $endChapter = null, $zip = true)
    {
        $provider = $this->determineProvider($url);

        if ($provider === false) {
            throw new NoProviderFoundException();
        }

        $provider->setUrl($url);

        if ($endChapter === null) {
            $chapters = $provider->getChapters();
            $endChapter = end($chapters);
        }

        if ($startChapter === null) {
            $startChapter = 1;
        }

        for ($i = $startChapter; $i <= $endChapter; $i++) {
            $chapterName = $provider->getChapterName($i);
            $chapterPath = $this->getChapterPath($chapterName, $zip);
            if (
                !file_exists($chapterPath)
                && !is_dir($chapterPath)
            ) {
                $this->fetchChapter(
                    $provider->getChapter($i),
                    $chapterName,
                    $zip
                );
            }
        }
    }

    /**
     * @return string
     */
    public function getSavePath()
    {
        return $this->savePath;
    }

    /**
     * @param string $savePath
     */
    public function setSavePath($savePath)
    {
        $this->savePath = $savePath;
    }

    /**
     * Fetch chapter to disk
     *
     * @param array $urls
     * @param string $name
     * @param bool $zip
     */
    protected function fetchChapter(array $urls, $name, $zip = false)
    {
        if (empty($urls)) {
            return;
        }

        $fs = new Filesystem();
        $dir = $this->getSavePath() . '/' . $name;
        $fileName = $this->getChapterPath($name, $zip);

        if ($fs->exists($fileName)) {
            return;
        }

        $fs->mkdir($dir);

        if (!count($urls)) {
            // Empty chapter, bail out!
            return;
        }

        foreach ($urls as $page => $image) {
            try {
                $content = $this->client
                    ->getClient()
                    ->get($image)
                    ->getBody();
            } catch (HttpClientException $e) {
                $fs->remove($dir);
                break;
            }

            if (empty($content)) {
                $fs->remove($dir);
                break;
            }

            $fs->dumpFile(
                sprintf(
                    '%s/%03d.%s',
                    $dir,
                    (int) $page + 1,
                    $this->determineExtension($image)
                ),
                $content
            );
        }

        if (true === $zip && $fs->exists($dir)) {
            $this->zipDir($dir, $fileName);
        }
    }

    /**
     * Create zip file from directory with chapter images
     *
     * @param $dir
     * @param $zipName
     */
    protected function zipDir($dir, $zipName)
    {
        $fs = new Filesystem();
        $zip = new \ZipArchive();

        $zip->open($zipName, \ZIPARCHIVE::CREATE) || die('Error creating zip: ' . $zip->status);
        $handle = opendir($dir);
        while ($file = readdir($handle)) {
            $filePath = realpath($dir . '/' . $file);

            if (is_file($filePath)) {
                $zip->addFile($filePath, basename($file));
            }
        }
        $zip->close();

        $fs->remove($dir);
    }

    public function getChapterPath($name, $zip)
    {
        $dir = $this->getSavePath() . '/' . $name;
        return ($zip === true) ? $dir . '.zip' : $dir;
    }

    /**
     * Determine file extension
     *
     * @param $url
     * @return string
     */
    protected function determineExtension($url)
    {
        return end(explode('.', end(explode('/', $url))));
    }

    /**
     * Determine Url provider. Returns false on failure.
     *
     * @param string $url
     * @return bool|UrlProviderInterface
     */
    protected function determineProvider($url)
    {
        foreach ($this->getProviders() as $provider) {
            /* @var UrlProviderInterface $provider */
            if ($provider->supports($url)) {
                return $provider;
            }
        }

        return false;
    }
}
