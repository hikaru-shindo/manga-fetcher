<?php
use Pimple\Container;
use MangaFetcher\Service\FetcherService;
use MangaFetcher\Command;
use Symfony\Component\Console\Application;
use Goutte\Client as GoutteClient;
use MangaFetcher\Provider;

$container = new Container();

$container['application'] = function (Container $c) {
    $application = new Application();

    $application->add(new Command\FetchCommand($c['fetcher.service']));

    return $application;
};

$container['goutte'] = function () {
    $client = new GoutteClient();
    return $client;
};

$container['fetcher.service'] = function (Container $c) {
    $fetcher = new FetcherService($c['goutte']);

    // Add registered prodivers
    $fetcher->addProvider($c['provider.mangareader']);

    return $fetcher;
};

$container['provider.mangareader'] = function(Container $c) {
    $provider = new Provider\MangareaderUrlProvider($c['goutte']);
    return $provider;
};

