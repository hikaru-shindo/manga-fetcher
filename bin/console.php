<?php
require_once realpath(__DIR__ . '/../vendor') . '/autoload.php';
require_once realpath(__DIR__ . '/../app/config/services.php');

use Symfony\Component\Console\Application;

/* @var $application Application */
$application = $container['application'];
$application->run();
